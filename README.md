# Backup Script

Script to backup a folder and also be able to maintain several previous versions efficiently

## Usage

```
Usage:
  backup [options] source destination

Options:
  -d <drive>  A drive to be mounted to destination before creating snapshot of source
  -n <num>    How many backups should be kept of source in destination (default: 3)
  -h          Show this screen.
```
